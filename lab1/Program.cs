﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab1
{
    class Program
    {
        /// <summary>
        /// нахождение длины диагонали прямоугольного треугольника
        /// </summary>
        /// <param name="k1">первый катет</param>
        /// <param name="k2">второй катет</param>
        /// <returns>длина диагонали</returns>
        static double d(double k1, double k2)
        {
            return Math.Sqrt(k1 * k1 + k2 * k2);
        }


        /// <summary>
        /// Главная функция программы
        /// </summary>
        /// <param name="args">Аргументы</param>
        static void Main(string[] args)
        {
            Console.WriteLine(d(2,3));

            Console.ReadKey();
        }
    }
}
